---
title: Arcturus
font_family: Arcturus
project_url: https://gitlab.com/erg-type/foundry/arcturus
license: SIL OPEN FONT LICENSE
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web
designers:
 - [Laurent Müller, https://cargocollective.com/mullerlaurent]
designer_url: 
styles:
 - Book
paths:
 - fonts/Arcturus/ArcturusBook.otf
tags:
 - Serif
ascender: 750
descender: -250
sample_text: Parfois j’ai la flemme de regarder les étoiles.
datem: 2024-05-15
public: yes
---

