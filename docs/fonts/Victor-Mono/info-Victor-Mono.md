---
title: Victor Mono
font_family: Victor Mono
project_url: http://victor.com
license: SIL
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - [victor, ]
styles:
 - Thin Italic
 - ExtraLight Oblique
 - Bold
 - Light Italic
 - Medium Italic
 - Bold Oblique
 - SemiBold Oblique
 - Italic
 - Bold Italic
 - Medium Oblique
 - Thin
 - ExtraLight Italic
 - ExtraLight
 - Oblique
 - Light
 - SemiBold
 - Thin Oblique
 - Light Oblique
 - Regular
 - Medium
 - SemiBold Italic
paths:
 - fonts/Victor-Mono/VictorMono-ThinItalic.otf
 - fonts/Victor-Mono/VictorMono-ExtraLightOblique.otf
 - fonts/Victor-Mono/VictorMono-Bold.otf
 - fonts/Victor-Mono/VictorMono-LightItalic.otf
 - fonts/Victor-Mono/VictorMono-MediumItalic.otf
 - fonts/Victor-Mono/VictorMono-BoldOblique.otf
 - fonts/Victor-Mono/VictorMono-SemiBoldOblique.otf
 - fonts/Victor-Mono/VictorMono-Italic.otf
 - fonts/Victor-Mono/VictorMono-BoldItalic.otf
 - fonts/Victor-Mono/VictorMono-MediumOblique.otf
 - fonts/Victor-Mono/VictorMono-Thin.otf
 - fonts/Victor-Mono/VictorMono-ExtraLightItalic.otf
 - fonts/Victor-Mono/VictorMono-ExtraLight.otf
 - fonts/Victor-Mono/VictorMono-Oblique.otf
 - fonts/Victor-Mono/VictorMono-Light.otf
 - fonts/Victor-Mono/VictorMono-SemiBold.otf
 - fonts/Victor-Mono/VictorMono-ThinOblique.otf
 - fonts/Victor-Mono/VictorMono-LightOblique.otf
 - fonts/Victor-Mono/VictorMono-Regular.otf
 - fonts/Victor-Mono/VictorMono-Medium.otf
 - fonts/Victor-Mono/VictorMono-SemiBoldItalic.otf
tags: 
- monospace
ascender: 1100
descender: -250
public: yes
---

 