---
title: Espatarra
font_family: Espatarra
project_url: http://esperatta.com
license: SIL
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - [Jane Doe, http://jane.doe]
styles:
 - Regular
paths:
 - fonts/Espatarra/Espatarra.otf
tags: 
 - Serif
ascender: 1000
descender: -200
sample_text: "J'aime les cacahuètes mais je préfère les pistaches."
public: yes
---

