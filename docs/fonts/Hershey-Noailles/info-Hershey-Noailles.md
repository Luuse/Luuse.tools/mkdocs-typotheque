---
title: Hershey-Noailles-symbolic
font_family: Hershey-Noailles-symbolic
project_url: luuse.io
license: SIL
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - [Luuse, http://luuse.io]
 - [kikou, ]
styles:
 - Noailles-symbolic
paths:
 - fonts/Hershey-Noailles/Hershey-Noailles-symbolic.ttf
tags:
 - fantasy
ascender: 800
descender: -400
sample_text: "Récite moi l'alphabet."
public: yes
---

