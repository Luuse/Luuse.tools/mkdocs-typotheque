---
title: Libre Baskerville
font_family: Libre Baskerville
project_url: 
license: 
license_url: 
designers:
# - [designer name, designer url]
 - [, ]
styles:
 - Italic
 - Regular
 - Bold
paths:
 - docs/fonts/libre-baskerville/LibreBaskerville-Italic.ttf
 - docs/fonts/libre-baskerville/LibreBaskerville-Regular.ttf
 - docs/fonts/libre-baskerville/LibreBaskerville-Bold.ttf
tags:
ascender: 970
descender: -270
sample_text: "Récite moi l'alphabet."
public: no
---

 