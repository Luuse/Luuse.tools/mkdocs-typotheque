---
title: Arcturus
font_family: Arcturus
project_url: 
license: 
license_url: 
designers:
# - [designer name, designer url]
 - [, ]
styles:
 - Book
paths:
 - docs/fonts/Arcturus/ArcturusBook.otf
tags:
ascender: 750
descender: -250
sample_text: "Récite moi l'alphabet."
public: no
---

 