---
title: Kraft Mono
font_family: Kraft Mono
project_url: http://kraft.com
license: SIL
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - [edouard, ]
 - [John Doe, ]
styles:
 - Italic
 - Italic
 - Regular
paths:
 - fonts/kraft-mono/KraftMono-Italic.otf
 - fonts/kraft-mono/KraftMono-Italic.ttf
 - fonts/kraft-mono/KraftMono.otf
tags: 
 - monospace
ascender: 750
descender: -250
public: yes
---
