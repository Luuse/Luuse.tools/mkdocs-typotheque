---
title: HK Grotesk
font_family: HK Grotesk
project_url: grotesk.com
license: SIL
license_url: https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=ofl
designers:
 - [jean, jean.fr]
styles:
 - SemiBold
 - Legacy Italic
 - Regular
 - Light
 - Bold Legacy
 - Medium Legacy Italic
 - Bold Legacy Italic
 - Medium Legacy
 - SemiBold Legacy Italic
 - Black
 - Light Italic
 - Medium Italic
 - Bold Italic
 - SemiBold Legacy
 - Medium
 - Regular Legacy
 - Light Legacy Italic
 - Italic
 - Light Legacy
 - Bold
 - SemiBold Italic
 - ExtraBold
paths:
 - fonts/hk-grotesk/HKGrotesk-SemiBold.otf
 - fonts/hk-grotesk/HKGrotesk-LegacyItalic.otf
 - fonts/hk-grotesk/HKGrotesk-Regular.otf
 - fonts/hk-grotesk/HKGrotesk-Light.otf
 - fonts/hk-grotesk/HKGrotesk-BoldLegacy.otf
 - fonts/hk-grotesk/HKGrotesk-MediumLegacyItalic.otf
 - fonts/hk-grotesk/HKGrotesk-BoldLegacyItalic.otf
 - fonts/hk-grotesk/HKGrotesk-MediumLegacy.otf
 - fonts/hk-grotesk/HKGrotesk-SemiBoldLegacyItalic.otf
 - fonts/hk-grotesk/HKGrotesk-Black.otf
 - fonts/hk-grotesk/HKGrotesk-LightItalic.otf
 - fonts/hk-grotesk/HKGrotesk-MediumItalic.otf
 - fonts/hk-grotesk/HKGrotesk-BoldItalic.otf
 - fonts/hk-grotesk/HKGrotesk-SemiBoldLegacy.otf
 - fonts/hk-grotesk/HKGrotesk-Medium.otf
 - fonts/hk-grotesk/HKGrotesk-RegularLegacy.otf
 - fonts/hk-grotesk/HKGrotesk-LightLegacyItalic.otf
 - fonts/hk-grotesk/HKGrotesk-Italic.otf
 - fonts/hk-grotesk/HKGrotesk-LightLegacy.otf
 - fonts/hk-grotesk/HKGrotesk-Bold.otf
 - fonts/hk-grotesk/HKGrotesk-SemiBoldItalic.otf
 - fonts/hk-grotesk/HKGrotesk-ExtraBold.otf
tags: 
 - sans-serif
ascender: 1000
descender: -303
public: yes
---
