## Install

`pip install mkdocs mkdocs-img2fig-plugin`

## start server 

`mkdocs serve --livereload`

├── fonts
│   └── not-courier-sans
│       ├── NotCourierSans-Bold.otf
│       └── NotCourierSans.otf
├── foundry
│   └── not-courier-Sans.md
├── index.md
├── live 
└── projets

## Change white background to alpha

`convert input.png -fuzz 10% -transparent white output.png`
