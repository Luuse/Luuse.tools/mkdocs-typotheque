var _BODY;
var _BG;
var _NAV;
var _COLOR;
var _ABOUT;

var title = document.querySelector("nav h1");
var about = document.getElementById("about");
var aboutClose = about.querySelector(".close");
var inputs = document.querySelectorAll('.sample-text');

title.addEventListener("click", function () {
  if (about.classList.contains("hidden")) {
    about.classList.remove("hidden");
  } else {
    about.classList.add("hidden");
  }
});

aboutClose.addEventListener("click", function () {
  about.classList.add("hidden");
});

inputs.forEach(function(input){

	input.addEventListener("input", function(){
		var test = input.innerHTML;

		inputs.forEach(function(input){

			input.innerHTML = test;

		});

	});

});

function loadColor() {
  var cookies = document.cookie;
  cookies = cookies.split(";");
  if (cookies.length > 0) {
    cookies.forEach(function (cookie) {
      name = cookie.split("=")[0].replace(" ", "");
      value = cookie.split("=")[1].replace(" ", "");
      _BODY[0].style[name] = value;
      _NAV[0].style[name] = value;
      _ABOUT[0].style[name] = value;

      if (name == "background") {
        _BG.value = value;
      }
      if (name == "color") {
        _COLOR.value = value;
      }
    });
  }
}

function changeColor(picker, elements, style_type) {
  picker.addEventListener("input", function () {
    elements.forEach(function (element) {
      element.style[style_type] = picker.value;
    });
    document.cookie = style_type + "=" + picker.value;
  });
}

document.addEventListener("DOMContentLoaded", function () {
  _BODY = document.querySelectorAll("body");
  _NAV = document.querySelectorAll('nav');
  _ABOUT = document.querySelectorAll('#about');
  _BG = document.getElementById("backColor");
  _COLOR = document.getElementById("textColor");

  changeColor(_BG, _BODY, "background");
  changeColor(_BG, _NAV, "background");
  changeColor(_BG, _ABOUT, "background");
  changeColor(_COLOR, _BODY, "color");
  changeColor(_COLOR, _NAV, "color");
  changeColor(_COLOR, _ABOUT, "color");
  loadColor();
});

var filters = document.querySelectorAll(".filter");
var tags = document.querySelectorAll(".value");
var fonts = document.querySelectorAll(".font");

filters.forEach((filter) => {
  filter.addEventListener("click", function () {
    var selected = filter.getAttribute("data-filter");
    if (filter.classList.contains("selected")) {
      filter.classList.remove("selected");
      hideFonts(selected);
    } else {
      filter.classList.add("selected");
      showFonts(selected);
    }
  });
});

function showFonts(selected) {
  tags.forEach((tag) => {
    var tagValue = tag.getAttribute("data-value");
    if (tagValue == selected) {
      tag.closest(".font").classList.remove("hidden");
      tag.closest(".font").classList.add("show");
    }
  });
  fonts.forEach((font) => {
    if (!font.classList.contains("show")) {
      font.classList.remove("show");
      font.classList.add("hidden");
    }
  });
}

function hideFonts(selected) {
  var i = 0;
  filters.forEach((filter) => {
    i = filter.classList.contains("selected") ? i + 1 : i;
  });

  fonts.forEach((font) => {
    if (i == 0) {
      font.classList.remove("show");
      if (!font.classList.contains("show")) {
        font.classList.remove("hidden");
      }
    } else if (i > 0) {
      tags.forEach((tag) => {
        var tagValue = tag.getAttribute("data-value");
        if (tagValue == selected) {
          tag.closest(".font").classList.remove("show");
          tag.closest(".font").classList.add("hidden");
        }
      });
    }
  });
}
